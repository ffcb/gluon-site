# site-ffcb
site.conf für das Release 0.7 / v2016.1.4
Und am ende fallen dann alle images für das target ar71xx-generic raus. das sind die wichtigsten.
Für weitere targets füre einfach folgendes nach dem make aus:

make GLUON_TARGET=target-name

verfügbare Targets sind:
ar71xx-generic
ar71xx-nand
mpc85xx-generic
x86-generic
x86-kvm_guest

## Changelog
### 20160509
- Anpassung Site-Dateien nach Fork
- Versionsnummer in site.mk auf 0.x.x.x eingestellt